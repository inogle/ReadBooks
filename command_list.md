# GIT command
###PULL
<code>git pull origin develop</code>

###FETCH
<code>git fetch</code>

###STASH
<code>git stash</code>

###POP
<code>git stash pop</code>

###COMMIT
<code>git add .</code>
<code>git commit -m "comment"</code>

###PUSH
<code>git push origin develop</code>

###PULLプッシュした時のミスを消す
<code>git revert -m 1 commitId</code>

###RESET (delete commit)
<code>git add .</code>
<code>git reset --soft HEAD^</code>

###RESET (hard)
<code>git add .</code>
<code>git reset --hard HEAD^</code>

###GIT CLONE
<code>git clone http://~~</code>

###OTHER
<code>git status</code>
<code>git branch</code>
<code>git branch -a</code>
<code>git log</code>
使ったこと無い<code>git tag</code>
<code>git checkout -b "branchName"</code>

###CONFIRM BRANCH (別途導入処理が必要)
<code>git graph</code>

###NOT TRIED
使ったこと無い<code>git diff</code>

### TAR 解凍コマンド
<code>tar -zcvf. xxxx.tar.gz directory</code>
<code>tar -zxvf. xxxx.tar.gz</code>

#ECLIPSE
### MavenProfileを指定してBUILD(for ark)
<code>mvn clean install -Dmaven.test.skip=true -P UT</code>

#WILDFLY

#file operations
###scp command リモートTOローカル
<code>scp root@address:serverlist.txt ~/materials</code>
<For example>
<code>scp root@10.157.99.36:/var/lib/jenkins/workspace/tor-PR/materials.tar.gz ~/materials</code>

###必要なフォルダを一気に作ってくれる。
<code>mkdir -p /usr/local/sb/weir</code>

### wildfly 再起動コマンド
<code>service wildfly_01 restart</code>
<code>cd /etc/init.d/</code>
にwildflyが入っているため使える。

### psqlコマンド
<code>psql -h 10.157.17.171 -U user_training -d db_training</code>

### sshコマンド
<code>ssh root@10.157.82.101</code>

### 取り敢えず全員に書き込み権限まで与えるchownコマンド
<code>chown 777</code>

### telnetコマンド
<code>telnet ip port</code>
ex: <code>telnet 10.157.16.207 8823</code>

### gzファイルを解凍せず中身を見る
<code>zcat ~~.gz</code>

### MVNのSettingファイルを設定してclean install する方法
<code>mvn -s /usr/local/sb/mvn/conf/settings_Jedi.xml clean install -Dmaven.test.skip=true -X</code>

### 以下のオプションでEncodingを指定することも可能
使ったこと無い<code>-Dproject.build.sourceEncoding=UTF-8</code>

### RemotePushしていないものを消してしまった時にコミットログから復旧する方法
<code>git reflog</code>
<code>git reset --hard commitID</code>

### 複数ユーザでGitやるときにどのユーザでコミットするかを設定するコード
<code>git remote set-url origin https://TakuyaHoriuchi@github.com/TakuyaHoriuchi/codecheck.git/</code>

### (?)ファイル単位での変更履歴を参照する、
<code>git log -p app/view/hoge/show.html.erb</code>
### gitの監視下から外す。
<code>git rm --cached softbank-training_challenge_1/.track/config.json</code>

### scp コマンドでリモートのファイルをローカルに落とす方法
<code>scp root@10.157.82.89:/var/lib/jenkins/workspace/tor-PR/materials.tar.gz ~/materials</code>

### springboot 起動コマンド
<code>./mvnw spring-boot:run</code>

docker run -d --name {コンテナ名} -e POSTGRES_PASSWORD={スーパユーザのパスワード} -p {コンテナへフォワードするホストのポート:フォワード先のコンテナのポート} postgres{:バージョン（指定しなければ最新）}

docker run -d --name postgres -e POSTGRES_PASSWORD=test -p 25432:25432 postgres

docker container ls

docker exec -ti {NAMES} bash
docker exec -ti postgres bash

psql --version

heroku 複数アカウント
https://workabroad.jp/posts/2118
単体テスト


画面コンテンツのデプロイ

cd ~/git/cme-ui-service-iTunesCDD/cicd/ansible
ansible-playbook -i inventories/local deploy.yml

UT 実行

cd ~/git/cme-ui-service-iTunesCDD/cmecsiTunes-ui/test
./startUIUnitTest_dev.sh

テスト結果レポートの確認

~/git/cme-ui-service-iTunesCDD/cmecsiTunes-ui/test/ut_reports/index.html

動作確認


画面コンテンツのデプロイ

cd ~/git/cme-ui-service-iTunesCDD/cicd/ansible
ansible-playbook -i inventories/local_mock deploy.yml

動作確認

http://localhost:8081/itunes/html/entry.html


minify agregy ？

git remote set-url origin https://TakuyaHoriuchi@github.com/TakuyaHoriuchi/codecheck.git/