TCP/IPプロトコルで通信データをパケットに分解して、受け取った側で分割されたパケットを組み合わせている。
PrivateIP →
クラスA 10. 
クラスC 192.168 
クラスB 172.16. ~ 172.31

http://　　www.liit.jp　　/webtext/in.html
スキーム　　ホスト　　　　　　ぱす　　　
www.　　　　　iggkk.jp
ローカル名　　　ドメイン

<?php
    // get formdata
    $user = $_POST['user'];
    $pass = $_POST['pass'];
    // confirm
    if(strcmp($user, 'id') == 0 && strcmp($pass, 'pass') == 0) {
        header('Location: item_list.php');
    } else {
        header('Location: login_failed.html');
    }
?>

Cookieはブラウザ内にストレージを持つことが出来るし、CookieをセットしてRequestする時はHttp Headerを利用して中身を詰められる。
ただ、これは状態保存はされていないので、PASSWORDを使ったものをCookieを用いて行う事は危険である。
→Sessionというものを用意して状態を保存することで安全かつ少ないデータ量で通信することが可能になる。

正規化：DBのテーブル管理に置いて、できるだけ重複データを排除していくこと。(注文情報と標品一覧を別テーブルにする　等)

CGIプログラムとアプリサーバの違い
CGIの場合、Webサーバからのリクエストがあってからプロセスが立ち上がり、処理が終了したらプロセスがダウンする使い捨て方式
JVMプロセス上のアプリサーバの場合は、常にプロセスが実行されており、リクエストが北タイミングでServlet やJSPを起動する使い回しモデル。

Apache Tomcatの違い
Apache HTTP Server -> Webサーバ
Tomcat -> アプリケーションサーバ　：　Apacheからアプリサーバを読むきのうとして、ApacheにMod_jkという機能が与えられている。通信プロトコルはajp13
TOSE: Nginx -> wildflyは？
apache-tomcatの連携先Prameterを以下のように設定する。
例：
workers.properties
 worker.list=worker1            // 接続先のworkerの名前を定義、Tomcatワーカ 複数使用する時は worker.list=worker1, worker2 的にWrite
 worker.worker1.host=localhost  // 異なるノードへ転送する場合はここでIP指定
 worker.worker1.port=8009
 worker.worker1.type=ajp13
httpd.conf
 JkWorkersFile  /usr/local/apache2/conf/workers.properties
 JkLogFile      /usr/local/apache2/logs/mod_jk.log
 JkLogLevel     info
 JkMount        /webtext/pentomino/* worker1 // 転送先を設定している。/web~ → worker1 のように転送。 複数の場合ここは複数行になる。

Webサーバのように、機能を持っているので、Tomcat単体でも動作可能。転送設定も入れる必要なく楽な構成には出来る。
アプリサーバの機能：
セッション管理・トランザクション管理・DB接続の管理・Webアプリの管理
DB接続の管理：アプリサーバが代表してDBへの接続を確保しておき、Webアプリの必要に応じて接続を使い回すというConnectionPoolingの機能が用意されている。

web.xml でサーブレット定義を行い、サーブレットマッピングを行うことで、URLから遷移先を指定している。 J2EE仕様で決められた標準の定義ファイル

<!-- サーブレット定義 -->
<servlet>
    <servlet-name>login</servlet-name>
    <servlet-class>jp.littleforest.webtext.pizza.servlet.LoginServlet</servlet-class>
</servlet>
<!-- サーブレットマッピング -->
<servlet-mapping>
    <servlet-name>login</servlet-name>
    <url-pattern>login.do</url-pattern>
</servlet-mapping>
サーブレットClassはdoPostメソッドをなどをOverrideしてRequestDispatcherとかで遷移先を指定してforwardしてる。
forwardは遷移先リソースを取得して表示、
redirectはリダイレクト要求をもらい、再度リソースを取得しにアクセスすることを言う。

RequestScopeを使用する意図→SessionはHttpの仕様外でCookieなどの仕組みを利用して買ってに利用しているため。
全部セッションだとメモリくっちゃう
SessionTimeout→TomcatではDefault30分

SessionIdの受け渡し方法→Cookieを利用する方法、GET Parameter

Security
・SQLインジェクション対策
Validationを実施する。JDBCドライバを利用する場合、PreparedStatementを利用して実行するクエリ文を固定する。

・CrossSiteScripting XSS クロスサイトスクリプティング
項目にjsコードを入力すると、そのスクリプトが実行出来てしまう→Cookieの情報を抜き取るなどの処理を加えられてしまう。
→対策：サニタイジング→「&lt; →などの特殊文字を文字参照に置き換える。」
サニタイジングのタイミングは入力されたときではなく、出力時に出力対象に応じて実施するのが良い・

・セッションハイジャック
クライアントとサーバ間でやりとりされているセッションIDを第三者が盗み取ることで、利用者になりすませてサービスが利用出来てしまう攻撃のこと
→実施する必要のある対策
　１XSS対策：よく使われる手法
　２通信経路の暗号化 SSL化：ネットワーク盗聴による通信傍受によってIDを知られないように。暗号化することで第三者が見れなくなる。
　３セッションタイムアウト地の変更：被害を最小限にする
　４セッションIDのランダム化→IDを推測出来ないようにするため。

SSL仕組み
http → 80
https → 443
SSL通信する時は、Webサーバへサーバ証明書を要求。その中に公開鍵がある。

・クロスサイトリクエストフォージェリ
攻撃者が捏造したフォームから強制的に情報をサブミットさせる。
ログイン情報を知らなくても、ログインできていれば攻撃が可能。
→対策：トークン値の利用して遷移元が正しいかを判定する。
hash関数は1:1対応ではないが、Collisionの確率が非常に小さい・
→現在はより衝突確率が低いSHA-256の利用が推奨されている・

近年ではレインボークラックと呼ばれる手法によって文字種が限られる場合元の文字列をすばやく推測出来るようになった。
→Salt文字列を利用することでリスクをへらす。

・強制ブラウズ　（直リンクも対象）
　→その画面へアクセス出来る対象なのかを判定する処理の追加が必要。

・ディレクトリトラバーサル
　リクエストで渡された文字列を利用してシステム内のファイルを表示されるアプリで起こり得る。、内部ファイルにアクセスするパスやファイル名を指定して、内部のファイルを開いてしまう手法
例えば http:// ~~~?filename=a.htmlならばa.htmlを表示するといったものに
filename=../../etc/passwdとすると管理者パスワードが見れてしまうなど
ではfilename=aならどうか。これもだめ。../../etc/passwd ¥0 としてしまえば以降の文字列が無視されてしまうため。
対策：SQLインジェクションと同様にサニタイジングを行う。

Digest認証→AuthoriHeaderにパスワードを含まず利用可能

：：戻るボタン問題→http通信を行っていないため、意図しない２重が送信可能
対策：
・キャッシュ無効化（ブラウザ）
no-cacheとかをヘッダーに設定する。
・戻るボタン無効化
jsで制御する方法→jsが無効になってたら意味なし
・ワンタイムトークンの利用


：：Wサブミット問題
・ボタンが押下されたかを変数としてもっておく
・１timeトークン

グローバル変数にユーザ情報もたせちゃ駄目→他のユーザの情報が見れてしまうから。
→ApplicationScopeなユーザ情報はもっちゃだめ

hiddenParamも意図した値化をチェック
デバッグ情報は画面に出力しない。




