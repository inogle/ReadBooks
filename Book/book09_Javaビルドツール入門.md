maven機能
- ビルド
- テスト
- ライブラリ
- デプロイ

Mavenの処理内容は、作業の目的を指定することで実行できる
Javaアプリ生成、Webアプリ生成、JavaDoc生成　など・・・

コマンド一覧
mvn compile
mvn test-compile
mvn test
mvn package パッケージ化
mvn clean 
mvn exec:java
mvn eclipse:eclipse Eclipseプロジェクトに変換する
mvn eclipse:to-maven　Ecli→Mavenプロジェクト
mvn install ローカルリポジトリーにライブラリを追加する。
    :install-file -Dfile=, groupId, arifactId, packaging, versionを指定することで指定のライブラリをインストールできる・

オリジナルのライブラリをローカルで使用できるようにしたとき、保存されている場所は、
HomeDir/.m2.repository