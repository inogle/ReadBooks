## Introduction

JVMとは、Javaを動かすために必要なソフトウェア
対象のOSで実行可能な形式のコードに変換して動かす」ことが出来る。

Java Program - JITコンパイラ - JVM - MacOS, Win, Linux
JVMがあればOSによらず動かせることが分かる。

JVMには３つの領域が存在する。
NEW, OLD Permanent
JVMのヒープ領域はオプションなどで自由に設定可能
クラスロードが大量の時はPermanent領域を増やす必要がある
New領域 → Eden From To領域に分けられる。

Objectがインスタンス化 → Eden
Edenがいっぱいになると、From、To領域に対比される。
→ScavengeGC
更にライフサイクルが長い情報をOld領域に格納する。
























