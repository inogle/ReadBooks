## 1 chapter
### Databaseが備えるべき条件
- 膨大データの一元管理
- データを複数の利用者で同時利用可能
- Performance
- 可用性：いつでも使える
- Security

SQLの分類
- データ操作DML(crud)
- データ定義言語DDL(rename, comment)
- transaction(commit, rollback, savepoint)
- データ制御言語DCL(grant, revoke)

ddlは暗黙的にコミットしている

DB ServerとClientのやり取りとしては
アプリやEnterpriseManagerDatabaseExpress、SQLClientなどが存在する。

DB作成で作られるファイル
・Data File
・REDO File
・制御ファイル

1 RDB
2 Column
3 Record
4 RDBMS
5 no
6 no
7 y
8 y
9 n
10 y
ooooo ooooo

## 2 chapter
Point OEMDEの使い方、SQL*PLUSの使い方、sqlplus nologの結果
EM(Oracle Enterprise Manager) っていう
EMには２種類あるが
Performance監視、構成管理、診断Cuning　が出来る

EM CloudControl
EM Database Express EM Express
<font color="red" size="5">2-1わかんね、あとで</font>

Cloud ControlのComponent
- Oracle Management Repository
　管理対象ホストから収集されたデータの格納先
- Oracle Management Service
　Server上で動くWebアプリ
- Target固有のPluginを持つOracle Management Agent
　情報収集プロセス？
- Cloud Control Console
　
EM Expressアーキ
EM ExpressServletにより「、
認証、セッション管理、圧縮、キャッシュなどのRequestが処理される。
EM Expressにアクセス出来なかった時次のステップを実行
- Listner起動の確認
- DispatcherParamにTCPプロトコルが適用されているか確認
 show parameter dispatchers
- DBMS_XDB_CONFIG.setHTTPSPortプロシージャを実行して、HTTPSポートを設定
exec DBMS_XDB_CONFIG.setHTTPSPort (5500);
- ポート確認
SELECT DBMS_XDB_CONFIG.getHTTPSPort FROM DUAL;

EM Expressではいろんな機能が出来る
 初期化パラメータ編集、表領域の管理
 UNDO管理、REDO管理、制御ファイルのBakup
 User管理、Rollかんり、Profile管理
 、ADDMによって検出されたパフォーマンスの結果と推奨事項の表示
、AWRで取得した統計の表示、SQLCuningAdvisorの表示

### 2-2 sqlplus
sqlplus sys/oracle as sysdba 管理者けんげん実行
sqlplus /nolog DBに接続しない。
sqlplus /nolog @sample.sql というふうにすれば
ファイルを読み込んで一気に実行出来る。

CONNECT sh/sh ログインコマンド
COLUMN row_name format a20 列の表示書式をしているコマンド

HOST pwd OSコマンド実行

1 x
OracleDBの管理が出来るWebベースのツール。Webブラウザ上でGUI操作でさまざまな作業が出来る。
2 x
DBMS_XDB_CONFIG.setHTTPSPort
3 x
select DBMS_XDB_CONFIG.getHTTPSPort from DUAL;
4 x Console上でDB管理などが出来る。
DBoperateの他にDB起動停止・BakupRecovery可能
5 x CONNECT, HOST, COLUMN ？？
- SQLコマンド
- PL/SQLコマンド
- SQL*PLUSコマンド
- OSコマンド

6 dbにアクセスせずsqlpを起動する

## 3 chapter schema obj
Schema：ユーザが作成した表を管理する仕組み
HRユーザを作成した時、HRスキーマというものが作成される。
部署表、あ表などを作成すると、HRスキーマに追加される。

Data Dictionary：DBを動かすために必要な情報が入ってる。
DB作成時に生成され、所有者は「SYS」USER

データ型種類
NUMBER CHAR{2000(byte or strLen)}
VARCHAR{4000(byte or strLen)}
DATE, TIMESTAMP(小数秒を含む)
BLOB, CLOB めっちゃでっかいバイナリData, 文字列

CHAR固定長なので空白文字で補完する
ABC__ 的な感じで検索時も指定必須
VARCHARは可変で補完なし
BLOB：画像音楽、CLOB：歌詞

FOREIGN KEYでエラーになるケースならないケース
→　参照元のデータを消す時に
参照先が存在する場合エラーとなり、参照しているレコードがなければOKとなる。
UNIQUE制約はNULLを許容する。

ALTER文などで列を追加するときは既存列の最後に追加される。
ALTER分などで列を削除した時戻すことは出来ない。
ALTER文などで列のサイズを変更しようとした場合、
　既に存在している文字が消去するときは、でーたが壊れてしまうため、失敗する、

削除した表に関係するスキーマオブジェクトは、
制約：一緒に削除
索引：一緒に削除
Trigger：一緒に削除
View：削除されないが、削除した表にアクセス出来ずINVALIDになる
ストアドプログラム：削除されないがアクセス出来ずINVALIDになる。

表の削除は、実データが物理削除されるのではなく、削除されたという情報をデータ・ディクショナリ表に追加されるだけなので、
フラッシュバックドロップという処理で復活させることが可能。
削除した表の復活の許可はRECYCLEBIN初期化パラメーたで設定できる。

sqlplus /nolog でログイン
connect scott
show recyclebin
flashback table products to before drop;
show recyclebin
select OBJECT_NAME, ORIGINAL_NAME, OPERATION, TYPE

通常の索引→B-tree索引
PRIMARY KEYやUNIQUEKEYを指定すると自動的に索引がはられる。

表にINSERT DELETE UPDATEが発生した時、メンテナンスが発生して、
その分の処理が同時に発生して遅くなる。
View: １つや複数の表に対する問い合わせを保存したスキーマオブジェクト。
view:
データを持たない。
viewに対する更新はViewの元の表に対して行われる。
Viewを使うことで、特定の行にしかアクセスできなくなるので、セキュリティ向上に効果はある
DML分はもととなる表に対して行われる。

シノニム：表などのスキーマオブジェクトにつける別名のこと。
ストアドプログラム；ソースコードとコンパル後のコードがDBに格納されたプログラ無の事をいう。
→PL/SQLという独自のプログラムを持っており、これの事をいう。
　→出位としては、
　　１プロシージャ、２ファンクション、３パッケージ、４トリガーが存在する。
　　A:::特定の処理を実行する。戻り値は一種類。
　　B:::VOID
　　C:::１　＋　２
　　
SQL*LOADER：ジャイルに記載されたデータをDB上のひょうに挿入するUtility。
INPUTにはData File, 制御ファイルが必要。
２種類の呼び方あり・
①従来型パスロード：一つ一つ処理を実行している。
②ダイレクトパスロード：最高水位標情報取得。
Data Pumpを使用して、異なるDB間ででーたを共有出来る。
export : expdp
import : impdp

- 1  y
n(X) 30 varchar2
- 2  n
o first str is english.
- 3  n
o 同じ名前はだめ
- 4  y
o
- 5  索引
x 一意索引
- 6  制約を課した結果、既存のレコードが制約を満たしていないとエラー
になる。
o
- 7  flash back
x flash back drop
- 8  索引・制約・トリガー
o
- 9  CASCADE
x CASCADE CONSTRAINTS
- 10 n
o
- 11 n
o
- 12 xxx
コピーされるものは、標構造、データ、NOT NULL制約
- 13 n
o
- 14 n
x 
- 15 y
x
- 16 y
o
- 17 参照先の表へ更新処理を行う。
o
- 18 ng
o
- 19 ?

- 20 n
x
- 21 従来型パスコード
o
- 22 ダイレクトパスコード
o


















