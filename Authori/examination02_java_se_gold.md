# 1.
○1 a
2 d
c
3 a
d
4 b
d
○5 bc
○6 d
7 b
a
8 c
b hashcodeメソッドはオブジェクトを検索する際のpf向上目的で使用される。
9 e a
b d finalはクラスでは継承不可、変数では再代入不可、メソッドではoverride不可。意味が少し違う。
10 c▲▲
d static→インスタンスは、生成されているとは限らないものを参照しているためコンパイルエラーとしている
11 d▲▲
c 初期化時はstatc{x=1;}とかを使おう
12 f a▲▲ 
df 
singleTon
public class Foo {
    private static final Foo foo = new Foo();
    private Foo(){}
    public static Foo getInstance() {
        return foo;
    }
}
外部から取得する際は Foo foo = Foo.getInstance();

13 ▲▲b
c 不変クラスとは一度設定された状態を変更できないよう設計されたクラス　Stirngとかラッパークラスがあてはまる
14 a c?
bc SE8でStaticメソッドやdefaultメソッドを定義できるようになった。
15 d
a 多重継承で同じメソッドを複数継承するとコンパイルエラー
16 c
a ダイヤモンド継承
17 d?
a 
18 ?c?
e 入れ子クラスは可能
19 b
d Outer.Inner inner = new Ounter().new Inner(); inner.doIt();
20 c
d new Foo(){public void doit();}
21 a
c
22 c
b enumクラスのサブのため、他のクラスの継承ができない。暗黙的にfinalとして定義しているため、finalやabstructは指定できない。
23 b
ac enumはnameやtostringがあってインスタンスを取得するためにはvalueofメソッドを使う。ordinalは定義順番値を返す
24 b
25 c
ac
○



# 2. 一巡目

# 3. 一巡目 JAVA判定ロジック

# 4.
1.cd
2.bd
cd
streamAPIを使用したデータ操作ステップ
データソースからストリームオブジェクトを取得
→　ストリーム帯ジェクトに対して中間操作を適用
→　ストリームオブジェクトに対して終端操作を適用
処理の対象をデータソースと呼ぶ
3.bc
aｃ　
4.b
a
5.d
Stream APIを使わない場合
List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
for (Integer i : integerList) {
    if (i % 2 == 0) {
        System.out.println(i);
    }
}
StreamAPIを使う場合
List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
integerList.stream() // streamの取得
        .filter(i -> i % 2 == 0) // 中間操作
        .forEach(i -> System.out.println(i)); // 終端操作
処理の違いを見てみましょう。
Stream APIを使わないパターンでは「2で割り切れたら表示」という構造になっています。
Stream APIを使うパターンでは「2で割り切れるものを集める」→「要素をすべて出力」という構造になっています。

6.d
forEachは終端操作用メソッドになるので複数回実行できない。
7.a
d syspln は引数を２つ宣言しないといけない。
MapのforEachメソッドはBiconsumerオブジェクトを渡す必要があり、これは引数が２つ必要
8.b
c　int型の配列からstreamするとIntStreamが取得できる
9.d
c
10.de
af range, rangeClosedでIntstreamの数値範囲ストリームを取得できる。
　機能はほぼ同じ、端っこを含むか否か
11.d
b　Streamオブジェクトを取得するためにstaticメソッドのofメソッドを用いる。
12.bc
bd　sortedメソッドで自然順序で並び替える。Comparatorオブジェクトのcompareメソッドをoverrideして実装。
13.d
14.a
15.a
c
16.d　後でもう一回！！！！！！！
c
17.a
c
18.acd
abc
19.d?
b
20.a
d LISTが２つはいってる入れ子構造になっている。のをフラットにすｒのがflatMapメソッド、
　distinctメソッドを適用し、重複を削除する。
21.f　後でもう一回！！！！！！！！！
d
22.a
b
23.c
e
24.c 後でもう一回！！！！！！！！！
a
25.c 
d

# 5. 一巡目
1.b
2.c
3.b
4.d
5.a
6.a
7.a
b
try-with-resources文に記入することで、リソースをfinallyでクローズする必要があったのが、
try()の中に書くと自動でクローズしてくれるようになった。

FileReader in = null;
FileWriter out = null;

try{
    in = new FileReader("in.txt");
    out = new FileWriter("out.txt");
    // ファイルの読み下記
} catch(IOE e) {
    // 例外処理
} finally {
    try{
        if(in !=null) {
            in.close();
        }
        if(out !=null) {
            out.close();
        }
    } catch (IOE e){
        // 例外処理
    }
}
が以下になる。

try(
FileReader in = new FileReader("in.txt");
FileWriter out = new FileWriter("out.txt");
){
    // ファイルの読み下記
} catch(IOE e) {
    // 例外処理
} 

8.c
a
9.b
c
try-with-resources文においてtryブロックの引数で利用可能なクラスは
java.lang.AutoCloseableインタフェースを継承する必要がある。
10.b
11.a
assert x == 1 : "Error Message";
12.d
c アサーション機能を有効にするためには-eaオプションが必要。

# 6. 一巡目
1.c
b
2.c
3.d
cd
4.bc
de
5.ae
be
6.d
c
7.b

8.b
d
9.c
d
10.c
d
11.c
a
12.d

13.b
ce
14.a
c
15.b 暗記
ace

日時の表現クラスは不変クラスのため、
フィールドが変化しているわけではなく、書き換えメソッドによって書き換わるのは
別の値が挿入された別オブジェクトであることに注意

Localdate.parse("2016-04-01")
日付や時間の単位を表すIFはjava.time.temporal.TemporalUnit
実装はjava.time.temporal.ChronoUnit
ZoneId.systemDefault()

DateTimeFormatter formatter = 
DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
DateTimeFormatter.BASIC_ISO_DATE;
ofPatternで独自のパターンを指定してインスタンスを生成できる。

# 7. 一巡目
1.ab
ce java.io.fileクラスはUNIX系ファイルシステム固有の機能を利用できない
ファイルの所有者や属性などを取得変更できない
ファイル作成方法
File file = new File("/tmp/sample.txt");
2.d
c
3.c
4.c
5.b
Decoratorの復習必要
6.a
b データの出力専用クラス：PrintStream PrintWriter
try(PrintWriter writer = new Printwriter("out.txt")) {
    writer.println("hello");
}catch(Exception e) {
    e.printStackTrace();
}
7.bc
ab
8.c
Consoleはクラスで読みおtることができる
COnsole console = System.console();
9.b
d
10.b?d
JVMを意識した開発
Javaアプリが実行されている間、メモリ上のデータはJVMによって管理
ｊAVAアプリ終了後の維持できるStatic管理
JVMが利用可能なヒープ領域不足のときに、非アクティブなオブジェクトをHDDに対比させ必要なときにメモリ上に復元する。
ネットワーク越しで別アプリに転送する機能
<font color="red">Serializableクラスは直列可能かをマーキングするためのInterface
11.d
12.b
c trancientやstatic領域はJVM領域の復元要素、直列化要素にならない。

13.c
d
14.b
デフォルトのファイルシステムを表す帯ジェクト
c FileSystem fileSystem = FileSystem.getDefault();
15.ac
bd 
16.c
d
17.b
d
18.f
b path subpath(int beginIndex, int endIndex);
19.c
e  pathインタフェースのresolveメソッドの挙動
相対パスは結合パスを返し、絶対パスは渡されたパスそのものを返す。
20.c 
21.d
22.d
23.b
24.bc
cd
25.adf
bcd Basic, Posix, Dos FileAttributes interfaceが提供されている。
java.io.file.attributeパッケージには。ファイル属性を表すために。
26.a
27.c
d
28.d　<font color="red">＠復習
c walkFreeTreeメソッドはディレクトリ階層の再帰的なトラバース処理を行うために用意されている。
29.a
b
30.a
31.a
b
本の間だけを立たせることができた！！　笑

# 8. 並行処理
1.abce
abde
並行処理ユーティリティは並行処理のライブラリ群
synchronizedListとかの同期化ラッパーは並行処理→同期化コレクション

カウンティング・セマフォ：
セマフォとは信号装置という意味。有限のリソースに対して並列的にアクセスする
プロセスやスレッド感における動機や割り込み制御に用いられる仕組み。
java.util.concurrent.Semaphoreクラスによって有限リソースに対して
並行アクセス可能なスレッド数を自由に設定できる仕組みを提供

並行コレクション：
従来の同期化コレクションでは状態へのアクセスを直列化してスレッドセーフを実現していた。
がこれによって複数スレッドからの並列アクセスを前提に設計している。

アトミック変数：
原始性、それぞれの処理が不可侵であることを表す概念

スレッドプール：
アプリケーションで必要となるスレッドを予め生成し、プールしておくための仕組み
スレッド生成におけるオーバーヘッドを軽減し、アプリケーションで必要なスレッドの管理性を向上させることができる。
<font color="red">スレッド生成の仕組みを調べる。

2.a
b
スレッドセーフなステートであるフィールドを持つクラスを作成するときの型
→AtomicBoolean, Integer, Log. Reference
3.cde
acd
4.acd
abd
5.bc
6.a?b
c
JavaUtilパッケージで提供されている多くのコレクションクラスはシングルスレッドを前提とされた設計。
ArrayListやHashMapはスレッドセーフではない。
<font color="red">ロック・ストライピング
と呼ばれる細粒度のロック方式を用いて並行処理における実行性能の最適化

ConcurrentHashMap スレッドセーフなMapの実装クラス
CopyOnWriterArrayList arrayList

CyclicBarrier スレッドパーティ内の各スレッドの足並みをあわせるための機能を提供する
各スレッドが全てバリアに到達しないと先に進めない仕組みを設定できる。
Thread クラスを利用してタスクごとにスレッドを生成して、複数のタスクを非同期に実行できる。

Excecutor(FreamWork)
ScheduledThreadPoolExcecutorクラスを使用して、タスクのスケジューリング（遅延開始と周期的実行）を実現できる。
Runnable
Executorを使用してタスクを実行するには、
具象Executorオブジェクトを生成し、タスクを引数に渡してexecuteメソッドを実行する。
public class MyTask implements Runnable {
    public void run() {
        sysout("ok");
    }
}
従来のスレッドクラスを使用したタスク実行
Runnable task = new MyTask(); // make task 
Thread thread = new Thread(task); // make thread
thread.start(); // スレッドの実行

Executorを使用したタスク実行
public class MyExecutor implements Executor {
    @Overrride
    public void execute(Runnable command) {
        new Thread(command).start();
    }
}

Runnable task = new MyTask(); // make task 
Executor executor = new MyExecutor(); // 具象Executorを生成。
or, Executor executor = Executors.newScheduledThreadPool(3); // ExecutorsのFactoryMethodによる生成。
executor.execute(task); // スレッドの実行


Callable
Future<V>
7.bc
ad 
8.bce
bde
9.ce
bd
10.b
c
11.ac
bd
簡単にCallableを使用する例
public class MyCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        return "MyCallable: OK";
    }
}
ExecutorService service = Executors.newSingleThreadExecutor();
future<String> future = service.submit(new MyCallable());
sysout("Main: OK");
sysout(future.get()); // => MyCallable: OK

12.bc
13.ce <font color="red">＠
cd
14.bc 
Fork/Joinフレームワーク：大きなタスクを小さなタスクに分割し、分割したタスクを
複数スレッドで同時並行的に実行することで処理PFを向上させる仕組み。
非同期でタスクを実行できる。
ForkJoinTask, ForkJoinPool

15.b
16.c
bce
17.ab
18.c
19.c
OptionalInt opt = IntStream.of(1, 2, 3);
.parallel().findAny();
System.out.println(opt.getAsInt());

# 9. JDBC
1.c
２つから構成
jDBC API：JavaプログラムからDBにアクセスするためのAPI
JDBCドライバ：個々のDBに実際にアクセスするコードの実装
2.b
a
3.a
c 
Jdbcを使用してDBに接続するURL
jdbc:xxx//host[:port]/dbname[?option]
①    ②   ③
①プロトコル
②DB製品名
③接続の詳細

4.d
5.b
6.b?d
bc
7.e
f カーソル移動させないと読み取ってくれないよ。
8.c
9.b
c 戻り値があるかないかでtrue false!
10.a
d 持つことができるResultSetは一つだけ、２つ目を取得する直前に１つ目が閉じる。
11.a <font color="red">暗記
d
12.aef
ade
13.a
14.a
d updateRow()をしないと基のDBには更新されない。
15.c
b
16.c

# 10. Localize
1.c
2.acd
cef
3.c
d
4.c
Locale.Builderによってオブジェクトを生成できる。
Locale.Builder builder = new Locale.Builder();
builder.setLanguage("cat");
builder.setRegion("ES");
Locale locale = builder.build();

builder.setLanguage("123");
みたいに間違えた値をセットしてしまった場合は
illformedLocaleExceptionをスローします。

5.a
6.bd
ad
7.d
8.ad
cd
9.a
b
10.c
d
11.d
12.d
e
13.b
14.b
a
ListResourceBundle
リソース作成
public class SpanishResources extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        reutnr new Object[][] {
            {"Thank you.", "Gracias."},
            {"You're welcome.", "De nada."}
        }
    }
}
リソース適用
ResourceBundle rb = ResourceBundle.getBundle("q14.SpanishResorces");
sysout(rb.getString("Thank you.")) // => Gracias.
sysout(rb.getString("You're welcome.")) // => De nada.


リソースバンドルとは
主にUIにおける様々な表示項目で使用される言語や表示形式をロケールごとに１つにまとめたもの。
ロケールに応じてリソースを簡単に変更できる仕組みをアプリに組み入れられる。
<font color="red">開発で使えそう・・・



